public class Student {
    private String name;
    private int age;

    public void setName(String newname){
        name = newname;
    }
    public String getName(){
        return name;
    }
    public void setAge(int newAge){
        age = newAge;
    }
    public int getAge(){
        return age;
    }


}
